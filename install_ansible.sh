#! /bin/sh

# Get ansible installed
# ---------------------

sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible

sudo sh -c 'echo localhost ansible_connection=local > /etc/ansible/hosts'

# Now get our ansible requirments/libraries
sudo ansible-galaxy install -r requirements.yml

# Required for neovim
sudo apt-get install python-pip python3-pip -y




