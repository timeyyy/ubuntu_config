#!/bin/sh

# Finishing up (most stuff is in here because it requires input)
# --------------

# BG
# cd ~/Pictures
# wget http://ultrawidewallpapers.com/wp-content/uploads/2015/12/time-machine-3440x1440.jpg -O bg.jpg
# gsettings set org.gnome.desktop.background picture-uri "file:///$HOME/Pictures/bg.jpg"

# Neovim
pythonz install 2.7.9
pythonz install 3.12.1
mkvirtualenv -p $(pythonz locate 2.7.9) py2nvim
pip install pynvim
mkvirtualenv -p $(pythonz locate 3.12.1) py3nvim
pip install pynvim

echo "zsh being setup as defaul... requires logout"
sudo chsh -s $(which zsh) # set shell as default for root user
chsh -s $(which zsh) # for current user

# Zim
# cd ~
# git clone --recursive https://github.com/Eriner/zim.git ~/.zim

dropbox start -i
# Now configure the accounts to sync..

# cd $APP_SOURCE/android-studio/bin/ && ./studio.sh
# sudo apt-get -y install mysql-server
# sudo mysql_secure_installation

nvim -c "PlugInstall|qa"

echo "Please enter yadm decrypt password from keepass"
yadm pull
yadm decrypt


jetbrains-toolbox

# https://gadfly361.github.io/gadfly-blog/posts-output/2016-11-13-clean-install-of-ubuntu-to-re-natal/
