#! /bin/sh

# This init file should only be run when yadm is installed for the first time

yadm clone git@bitbucket.org:timeyyy/dotfiles.git
# Overwrite any files
yadm reset --hard origin/master
