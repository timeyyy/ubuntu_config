#!/bin/sh

# Standalone installer for Unixs

sudo apt-get update -y
sudo apt-get upgrade -y

SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
HERE=$(dirname "$SCRIPT")

# check git command
type git || {
  echo 'Installing git'
  sudo apt-get install -y git
}
echo ""

# Make dirs
-----------
APP_APPIMG=~/Apps/appimage # Downloaded appiamges
APP_REPO=~/Apps/repo         # Either git clones or builds of git clones
APP_SOURCE=~/Apps/source   # Compiled/Interpreted source files
APP_BIN=~/Apps/bin         # binaries
PROG=~/programming
PROG_REPO=~/programming/repo
TMP=~/tmp
mkdir $APP_APPIMG -p
mkdir $APP_REPO -p
mkdir $APP_SOURCE -p
mkdir $APP_BIN -p
mkdir $PROG -p
mkdir $PROG_REPO -p
mkdir $TMP -p

# Install Deps
--------------
sudo apt install -y snapd
sudo systemctl enable --now snapd.socket #ensure snapd runs on boot

# Neovim Deps
sudo apt-get install ninja-build gettext cmake unzip curl build-essential -y

# ZSH
sudo apt-get install yodl -y

# Clang
cd $APP_SOURCE
wget -O - http://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -
sudo apt-add-repository -y "deb http://apt.llvm.org/bookworm/ llvm-toolchain-bookworm-19 main"
sudo apt-get update -y
sudo apt-get install clang-19 lldb-19 -y
 This enables clang to call clang-19
sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-19 100
# This enables llvm-symbolizer to call llvm-symbolizer-19
sudo update-alternatives --install /usr/bin/llvm-symbolizer llvm-symbolizer /usr/bin/llvm-symbolizer-19 100

# Pythonz
sudo apt-get install build-essential zlib1g-dev libbz2-dev libssl-dev libreadline-dev libncurses5-dev libsqlite3-dev libgdbm-dev libdb-dev libexpat-dev libpcap-dev liblzma-dev libpcre3-dev libffi-dev tk-dev libssl-dev -y

# Add Repos
# sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main universe restricted multiverse" -y

# Mysql
# sudo apt-get install libmysqlclient-dev -y

# Install Applications
----------------------
# Zsh
sudo apt-get install -y zsh

# VSCode
# cd ~/Downloads
# wget https://go.microsoft.com/fwlink/?LinkID=760865 -O vscode64.deb
# sudo dpkg -i vscode64.deb
# sudo apt-get install -f -y # Install dependencies
# sudo apt-get install apt-transport-https -y
# sudo apt-get update -y
# sudo apt-get install code-insiders -y # or code

# Neovim
cd $PROG_REPO
# TODO - this blocks because git ssh fingerprint...
git clone git@github.com:neovim/neovim.git
cd neovim
git checkout v0.10.2
make CMAKE_BUILD_TYPE=Release CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=$APP_REPO/neovim"
make install
git remote set-url origin git@github.com:timeyyy/neovim.git
git remote add upstream git@github.com:neovim/neovim.git
mkdir -p ${HOME}/.asan_logs

cd $APP_REPO
git clone git@github.com:universal-ctags/ctags.git
cd ctags
./autogen.sh
./configure
make
sudo make install

# pip3 install jedi
# pip install jedi

# sudo apt-get install flex -y
# sudo apt-get install bison -y
# cd $APP_REPO
# git clone https://github.com/doxygen/doxygen.git
# cd doxygen
# mkdir build
# cd build
# cmake -G "Unix Makefiles" ..
# make
# sudo make install

# FZF
cd $APP_REPO
git clone --depth 1 https://github.com/junegunn/fzf.git fzf
fzf/install --completion --key-bindings --no-update-rc

# Vim Plug
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Rip Grep
sudo apt-get install -y ripgrep

# Dropbox
# https://www.dropbox.com/help/desktop-web/linux-repository
sudo apt install -y nautilus-dropbox

# Owncloud
# https://software.opensuse.org/download/package?project=isv:ownCloud:desktop&package=owncloud-client
# sudo sh -c "echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/desktop/Ubuntu_18.04/ /' > /etc/apt/sources.list.d/isv:ownCloud:desktop.list"
# wget -nv https://download.opensuse.org/repositories/isv:ownCloud:desktop/Ubuntu_18.04/Release.key -O Release.key
# sudo apt-key add - < Release.key
# sudo apt-get update -y
# rm Release.key
# sudo apt-get install owncloud-client -y

# Spotify
sudo snap install spotify

# Skype
# sudo snap install skype --classic

# Chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list'
sudo apt-get update -y
sudo apt-get install google-chrome-stable -y

# Keepass2
sudo apt-get install keepass2 -y

# VERACRYPT
sudo add-apt-repository ppa:unit193/encryption -y
sudo apt update -y
sudo apt install veracrypt -y

# Yet another dotfiles manager
sudo apt-get install yadm -y
yadm clone git@bitbucket.org:timeyyy/dotfiles.git
# This overwrites any files
yadm reset --hard origin/master

#Powerline
sudo apt install fonts-powerline -y

# Dev
# ----------------------
sudo apt-get install trash-cli -y
sudo apt -y install python3-virtualenv python3-virtualenvwrapper
# pip3 install --user virtualenv virtualenvwrapper

# NODE
# wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
# nvm install 8.10

# Clojure
# wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein -O lein
# Use 2.7.1 because newer version have stronger security checks
# wget https://raw.githubusercontent.com/technomancy/leiningen/2.7.1/bin/lein -O lein
# sudo chmod a+x lein
# mv lein $APP_BIN/
# lein version

#
# DATOMIC_VERSION="0.9.5561.54"
# cd $APP_SOURCE
# wget --http-user=tim_eichler@hotmail.com --http-password=271811af-2e6b-4e54-91fd-00b62c61ce1b https://my.datomic.com/repo/com/datomic/datomic-pro/$DATOMIC_VERSION/datomic-pro-$DATOMIC_VERSION.zip -O datomic-pro-$DATOMIC_VERSION.zip


# Nim
curl https://nim-lang.org/choosenim/init.sh -sSf | sh
choosenim stable -y

# Nice repl experience with readline
sudo apt-get install rlwrap

# Watchman
#cd $APP_REPO
#git clone https://github.com/facebook/watchman.git
#cd watchman
#git checkout v4.9.0  # the latest stable release
#./autogen.sh
#./configure
#make
#sudo make install
#echo 256 | sudo tee -a /proc/sys/fs/inotify/max_user_instances
#echo 32768 | sudo tee -a /proc/sys/fs/inotify/max_queued_events
#echo 65536 | sudo tee -a /proc/sys/fs/inotify/max_user_watches
#watchman shutdown-server
#sudo apt-get install -y libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1

# Install android studio
# cd $APP_SOURCE
# curl -sL https://dl.google.com/dl/android/studio/ide-zips/2.3.2.0/android-studio-ide-162.3934792-linux.zip -O android.zip
# unzip android-studio-ide-162.3934792-linux.zip
# rm android-studio-ide-162.3934792-linux.zip
# Some stuff required for the friggen emulator
# sudo apt-get install lib64stdc++6:i386 -y
# sudo apt-get install mesa-utils -y
# cd $ANDROID_HOME/emulator/lib64
# mv libstdc++/ libstdc++.bak
# ln -s /usr/lib64/libstdc++.so.6  libstdc++

# Slack
# sudo snap install slack --classic

# Pythonz
export PYTHONZ_ROOT=$APP_REPO/pythonz
curl -kLO https://raw.github.com/saghul/pythonz/master/pythonz-install
chmod +x pythonz-install
./pythonz-install
rm pythonz-install

# Java
# sudo apt-get install default-jre -y
# sudo apt-get install default-jdk -y

# Jetbrains toolbox
sudo apt install -y fuse

cd $APP_BIN
mkdir tmp
cd tmp
wget https://download.jetbrains.com/toolbox/jetbrains-toolbox-2.5.1.34629.tar.gz -O jb-toolbox.tar.gz
tar -xzvf jb-toolbox.tar.gz -C $APP_BIN
sudo ln -s $APP_BIN/jetbrains-toolbox-2.5.1.34629/jetbrains-toolbox /usr/bin/jetbrains-toolbox
cd ..
rm -r tmp


# Admin
# ------
sudo apt-get install xclip -y
sudo apt-get install tree -y

# Misc Apps
# ---------
sudo apt-get install unetbootin -y
sudo apt-get install ncdu -y # File size storage viewer


# Ledger Nano
# https://support.ledgerwallet.com/hc/en-us/articles/115005161925


# Misc Config
# -------------

# Battery Life
sudo apt install tlp -y

# Icons
#sudo apt-add-repository ppa:numix/ppa -y
#sudo apt-get update
#sudo apt-get install numix-gtk-theme numix-icon-theme-circle -y
#sudo apt-get install gnome-tweak-tool -y

dconf write /org/gnome/desktop/input-sources/xkb-options "['caps:escape']"
