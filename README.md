
wget https://bitbucket.org/timeyyy/ubuntu_config/raw/master/install.sh -O install.sh

FIRST: add ssh key to bitbucket and github: https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/

```
eval "$(ssh-agent -s)"

cd ~/.ssh
ssh-keygen -t rsa -b 4096 -C "timeyyy_da_man@hotmail.com" -f ~/.ssh/bitbucket -N ""
ssh-keygen -t rsa -b 4096 -C "timeyyy_da_man@hotmail.com" -f ~/.ssh/github -N ""
ssh-add ~/.ssh/bitbucket
ssh-add ~/.ssh/github

sudo apt-get install xclip -y

xclip -sel clip < ~/.ssh/bitbucket.pub
xclip -sel clip < ~/.ssh/github.pub
```

Github
------

In the upper-right corner of any page, click your profile photo, then click Settings. In the user settings sidebar, click SSH and GPG keys.

Bitbucket
---------
https://bitbucket.org/account/user/timeyyy/ssh-keys/

Commands to run
---------------

```
PROG_REPO=~/programming/repo
mkdir -p $PROG_REPO
sh ./install.sh $PROG_REPO/ubuntu_config
cd $PROG_REPO/ubuntu_config
sh work.sh

sudo shutdown -r now

sh ./end.sh
docker login
```

setup firefox login
setup dropbox selective sync


Sudo Nvim
---------
Edit the sudoers file with visudo:

sudo visudo

Look for the line that defines secure_path. If it’s restricting the PATH, you can either modify it or add the necessary paths to ensure that nvim can be found. For example:

Defaults    secure_path="/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin"

If necessary, add the full path to Neovim (e.g., /usr/local/bin/nvim) to the secure_path variable.



----------
TODO
make install-deps for all projects... (like neovim)
make backupclean (ready for a backup)


https://tech.aufomm.com/my-nix-journey-use-nix-with-ubuntu/
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
